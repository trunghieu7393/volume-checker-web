'use strict';  // eslint-disable-line

/**
 * Webpack configuration base class
 */
const fs = require('fs');
const path = require('path');
const webpack = require('webpack');

const npmBase = path.join(__dirname, '../../node_modules');

function excludeNodeModulesExcept (modules)
{
    var pathSep = path.sep;
    if (pathSep == '\\') // must be quoted for use in a regexp:
        pathSep = '\\\\';
    var moduleRegExps = modules.map (function (modName) { return new RegExp("node_modules" + pathSep + modName)})

    return function (modulePath) {
        if (/node_modules/.test(modulePath)) {
            for (var i = 0; i < moduleRegExps.length; i ++)
                if (moduleRegExps[i].test(modulePath)) return false;
            return true;
        }
        return false;
    };
}
class WebpackBaseConfig {

    constructor() {
        this._config = {};
    }

    /**
     * Get the list of included packages
     * @return {Array} List of included packages
     */
    get includedPackages() {
        return [].map((pkg) => fs.realpathSync(path.join(npmBase, pkg)));
    }

    /**
     * Set the config data.
     * This will always return a new config
     * @param {Object} data Keys to assign
     * @return {Object}
     */
    set config(data) {
        this._config = Object.assign({}, this.defaultSettings, data);
        return this._config;
    }

    /**
     * Get the global config
     * @return {Object} config Final webpack config
     */
    get config() {
        return this._config;
    }

    /**
     * Get the environment name
     * @return {String} The current environment
     */
    get env() {
        return 'dev';
    }

    /**
     * Get the absolute path to src directory
     * @return {String}
     */
    get srcPathAbsolute() {
        return path.resolve('./src');
    }

    /**
     * Get the absolute path to tests directory
     * @return {String}
     */
    get testPathAbsolute() {
        return path.resolve('./test');
    }

    /**
     * Get the default settings
     * @return {Object}
     */
    get defaultSettings() {
        const cssModulesQuery = {
            modules: true,
            importLoaders: 1,
            localIdentName: '[name]-[local]-[hash:base64:5]'
        };

        return {
            context: this.srcPathAbsolute,
            devtool: 'eval',
            devServer: {
                contentBase: ['./public/', './src/'],
                publicPath: '/assets/',
                historyApiFallback: true,
                hot: true,
                inline: true,
                port: 8000
            },
            entry: './index.js',
            module: {
                rules: [
                    {
                        enforce: 'pre',
                        test: /\.js?$/,
                        include: this.srcPathAbsolute,
                        loader: 'babel-loader',
                        query: {
                            presets: ['es2015', 'react', 'stage-2', 'stage-0']
                        }

                    },
                    {
                        test: /^.((?!cssmodule).)*\.css$/,
                        loaders: [
                            {loader: 'style-loader'},
                            {loader: 'css-loader'}
                        ]
                    },
                    {
                        test: /\.(png|jpg|gif|mp4|ogg|svg|woff|woff2|ttf|eot|ico)$/,
                        loader: 'file-loader'
                    },
                    {
                        test: /^.((?!cssmodule).)*\.styl$/,
                        loaders: [
                            {loader: 'style-loader'},
                            {loader: 'css-loader'},
                            {loader: 'stylus-loader'}
                        ]
                    },
                    {
                        test: /\.json$/,
                        loader: 'json-loader'
                    },
                    {
                        test: /\.cssmodule\.(sass|scss)$/,
                        loaders: [
                            {loader: 'style-loader'},
                            {
                                loader: 'css-loader',
                                query: cssModulesQuery
                            },
                            {loader: 'sass-loader'}
                        ]
                    },
                    {
                        test: /\.cssmodule\.css$/,
                        loaders: [
                            {loader: 'style-loader'},
                            {
                                loader: 'css-loader',
                                query: cssModulesQuery
                            }
                        ]
                    },
                    {
                        test: /\.cssmodule\.less$/,
                        loaders: [
                            {loader: 'style-loader'},
                            {
                                loader: 'css-loader',
                                query: cssModulesQuery
                            },
                            {loader: 'less-loader'}
                        ]
                    },
                    {
                        test: /\.cssmodule\.styl$/,
                        loaders: [
                            {loader: 'style-loader'},
                            {
                                loader: 'css-loader',
                                query: cssModulesQuery
                            },
                            {loader: 'stylus-loader'}
                        ]
                    },
                    {
                        test: /\.js?$/,
                        exclude: excludeNodeModulesExcept(['ccxt']),
                        use: {
                            loader: 'babel-loader',
                            options: {
                                extends: path.join(__dirname, '../../.babelrc'),
                                cacheDirectory: true
                            }
                        }
                    }
                ]
            },
            output: {
                path: path.resolve('./dist/assets'),
                filename: 'app.js',
                publicPath: './assets/'
            },
            plugins: [],
            resolve: {
                alias: {
                    api: `${this.srcPathAbsolute}/api/`,
                    assets: `${this.srcPathAbsolute}/assets/`,
                    components: `${this.srcPathAbsolute}/components/`,
                    constants: `${this.srcPathAbsolute}/constants`,
                    containers: `${this.srcPathAbsolute}/containers/`,
                    helper: `${this.srcPathAbsolute}/helper/`,
                    languageProvider: `${this.srcPathAbsolute}/languageProvider/`,
                    config: `${this.srcPathAbsolute}/config/${this.env}.js`,
                    reducers: `${this.srcPathAbsolute}/reducers/`,
                    store: `${this.srcPathAbsolute}/store/`,
                    styles: `${this.srcPathAbsolute}/styles/`
                },
                extensions: ['.js', '.jsx'],
                modules: [
                    this.srcPathAbsolute,
                    'node_modules'
                ]
            }
        };
    }
}

module.exports = WebpackBaseConfig;
