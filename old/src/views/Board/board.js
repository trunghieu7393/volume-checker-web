import React, {Component} from 'react';
import moment from 'moment';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

const cbRowProp = {
  mode: 'checkbox',
  clickToSelect: true,
};

function onAfterInsertRow(row) {
  let newRowStr = '';

  newRowStr += prop + ': ' + row[prop] + ' \n';
  alert('The new row is:\n ' + newRowStr);
}

const options = {
  afterInsertRow: onAfterInsertRow   // A hook for after insert rows
}

class Board extends Component {

  timeFormatter(cell, row) {
      return moment(cell).format('L');
  }
  render() {
      return (
          <div>
              <BootstrapTable ref='table' /*data={ this.state.coins }*/ selectRow={ cbRowProp } multiColumnSort={2} insertRow={ true } options={ options }>
                  <TableHeaderColumn dataField='symbol' dataAlign="center" isKey={true} filter={{
                      type : 'TextFilter',
                      delay : 500
                  }}>
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField='volume' dataAlign="center" ref='vol' dataSort={true} filter={{
                      type : 'NumberFilter',
                      delay : 500,
                      style : {
                          number : {display : 'none'},
                          comparator : {display : 'none'}
                      }
                  }}>Volume</TableHeaderColumn>
                  <TableHeaderColumn dataField='base' dataAlign="center" dataSort={true}>Base Price</TableHeaderColumn>
                  <TableHeaderColumn dataField='stoploss' dataAlign="center" dataSort={true}>Stop Loss</TableHeaderColumn>
                  <TableHeaderColumn dataField='target' dataAlign="center" dataSort={true}>Target Price</TableHeaderColumn>
                  <TableHeaderColumn dataField='profit' dataAlign="center" dataSort={true}>Current Profit</TableHeaderColumn>
                  <TableHeaderColumn dataField='source' dataAlign="center" dataSort={true}>Source</TableHeaderColumn>
                  <TableHeaderColumn dataField='note' dataAlign="left" dataSort={true}>Note</TableHeaderColumn>
                  <TableHeaderColumn dataField='status' dataAlign="center" dataSort={true}>Status</TableHeaderColumn>
                  <TableHeaderColumn dataField='timeStamp' dataAlign="center" dataFormat={this.timeFormatter} dataSort={true}> Time </TableHeaderColumn>
              </BootstrapTable>
          </div>

      )
  }
}

export default Board;
