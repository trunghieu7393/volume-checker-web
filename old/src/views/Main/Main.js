import React, {Component} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Aside from '../../components/Aside/';
import Footer from '../../components/Footer/';

import Dashboard from '../Dashboard/';

// ACCOUNT
import Portfolio from '../Account/Portfolio/';
import Volume from '../Volume/';

// Trade
import Exchange from '../Trade/Exchange/';
import Board from '../Board/';
import Bot from '../Trade/Bot/';

// Group
import GroupPage from '../Group/';
import Investing from '../Group/Investing/';
import FollowList from '../Group/Followlist/';
import Member from '../Group/Member/';
import Reference from '../Group/Reference/';

class Main extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              <Switch>
                <Route path="/volume" name="Volume" component={Volume}/>
                <Route path="/dashboard" name="Dashboard" component={Dashboard}/>
                <Route path="/portfolio" name="Portfolio" component={Portfolio}/>

                <Route path="/board" name="Buttons" component={Board}/>
                <Route path="/trade/exchange" name="Exchange" component={Exchange}/>
                <Route path="/trade/bot" name="Buttons" component={Bot}/>

                <Route path="/group" name="Group" component={GroupPage}/>
                <Route path="/group/investing" name="Investing" component={Investing}/>
                <Route path="/group/followList" name="FollowList" component={FollowList}/>
                <Route path="/group/member" name="Member" component={Member}/>
                <Route path="/group/reference" name="Reference" component={Reference}/>
                <Redirect from="/" to="/volume"/>
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Main;
