import React, { Component } from 'react';
import axios from 'axios';
import moment from 'moment';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import MarketController from '../../Controllers/marketController';
import CellFormatter from '../../utils/cellFormatter'


const URL = 'http://35.196.30.218:8080/api/v1.0/getAllCoinInfo';
// const URL = 'http://localhost:5000/api/v1.0/getAllCoinInfo';

function getVolumInfo() {
    return axios.get(URL).then(res => res.data.result);
}
class Volume extends Component {
    constructor(props) {
      super(props);
        this.state = {
          coins : []
        };
        this.options = {
          defaultSortName: 'percent',  // default sort column volume
          defaultSortOrder: 'desc',  // default sort order
          onRowClick: function(row){
          }
        };
         this.handleBtnClick = this.handleBtnClick.bind(this);
    }

    handleBtnClick(event) {
      let number = event.target.value;
      this.refs.vol.applyFilter({
          number : number,
          comparator : '>='
      })
    }

    calcCoinData(data) {
        let arrayCoins =[];
        Object.keys(data).forEach((key) => {
            let coin = data[key];
            arrayCoins.push(coin);
        })
        this.setState({ coins : arrayCoins });
    }

    // Override Method: run on Onload
    componentDidMount() {
      var countDown = 0;
      this.market = new MarketController("bittrex");

      var that = this;
      getVolumInfo().then((data) => {
        this.volumeInfo = data;
      })
      // run each 30s
      setInterval(function () {
        if(countDown == 0){
          countDown = 60;
          const currInfo = that.market.getInfoCoins().then(data =>{
            that.calcCoinData(data);
          });
        }
        countDown--;
        that.setState({ countdown : countDown });
      }, 1000);
    }
    render() {
        return (
            <div>
                <div>
                    <input type="number" onChange={this.handleBtnClick} placeholder="Volume Filter"/>
                </div>
                <p>Reload after: <i>{this.state.countdown}</i></p>
                <BootstrapTable ref='table' data={ this.state.coins } options={ this.options } multiColumnSort={2} striped hover condensed>
                    <TableHeaderColumn dataField='symbol' dataFormat={CellFormatter.linkFormat} dataAlign="center" isKey={true} filter={{
                        type : 'TextFilter',
                        delay : 500
                    }}>  </TableHeaderColumn>
                    <TableHeaderColumn dataField='quoteVolume' dataFormat={CellFormatter.thousand} dataAlign="center" ref='vol' dataSort={true} filter={{
                        type : 'NumberFilter',
                        delay : 500,
                        style : {
                            number : {display : 'none'},
                            comparator : {display : 'none'}
                        }
                    }}>Volume</TableHeaderColumn>
                    <TableHeaderColumn dataField='percent' columnClassName={ CellFormatter.numberColor } dataFormat={CellFormatter.percent} dataAlign="center" dataSort={true}>%V 1H</TableHeaderColumn>
                    <TableHeaderColumn dataField='percent' columnClassName={ CellFormatter.numberColor } dataFormat={CellFormatter.percent} dataAlign="center" dataSort={true}>%V 4H</TableHeaderColumn>
                    <TableHeaderColumn dataField='percent' columnClassName={ CellFormatter.numberColor } dataFormat={CellFormatter.percent} dataAlign="center" dataSort={true}>%V 1D</TableHeaderColumn>
                    <TableHeaderColumn dataField='change' columnClassName={ CellFormatter.numberColor } dataFormat={CellFormatter.percent}  dataAlign="center" dataSort={true}>%Price</TableHeaderColumn>
                    <TableHeaderColumn dataField='last' dataAlign="center" dataSort={true}>Last</TableHeaderColumn>
                    <TableHeaderColumn dataField='high' dataAlign="center" dataSort={true}>High</TableHeaderColumn>
                    <TableHeaderColumn dataField='low' dataAlign="center" dataSort={true}>Low</TableHeaderColumn>
                    <TableHeaderColumn dataAlign="center" dataFormat={CellFormatter.action} >Action</TableHeaderColumn>
                </BootstrapTable>
            </div>

        )
    }
}

export default Volume;
