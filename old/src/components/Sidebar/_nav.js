export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
      }
    },
    {
      title: true,
      name: 'Trade',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Volume Analysis',
      url: '/volume',
      icon: 'icon-rocket',
      badge: {
        variant: 'info',
        text: 'HOT'
      }
    },
    {
      name: 'Board',
      url: '/board',
      icon: 'icon-book-open'
    },
    {
      name: 'Exchange',
      url: '/trade',
      icon: 'icon-ghost',
      children: [
        {
          name: 'Buy/Sell',
          url: '/trade/exchange',
          icon: 'icon-wallet',
          badge: {
            variant: 'error',
            text: 'NOT WORKING'
          }
        },
        {
          name: 'Bot',
          url: '/trade/bot',
          icon: 'icon-magic-wand',
          badge: {
            variant: 'error',
            text: 'NOT WORKING'
          }
        }
      ]
    },
    {
      title: true,
      name: 'Account',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Portfolio',
      url: '/account/portfolio',
      icon: 'icon-pie-chart',
      badge: {
        variant: 'error',
        text: 'NOT WORKING'
      }
    },
    {
      name: 'Account Setting',
      url: '/account/accountsetting',
      icon: 'icon-settings',
      badge: {
        variant: 'error',
        text: 'NOT WORKING'
      }
    },
    {
      title: true,
      name: 'Group',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Investing',
      url: '/group/investing',
      badge: {
        variant: 'error',
        text: 'NOT WORKING'
      },
      icon: 'icon-briefcase'
    },
    {
      name: 'Follow List',
      url: '/group/followlist',
      icon: 'icon-notebook',
      badge: {
        variant: 'error',
        text: 'NOT WORKING'
      },
    },
    {
      name: 'Member',
      url: '/group/member',
      badge: {
        variant: 'error',
        text: 'NOT WORKING'
      },
      icon: 'icon-people'
    },
    {
      name: 'Reference',
      url: '/group/reference',
      badge: {
        variant: 'error',
        text: 'NOT WORKING'
      },
      icon: 'icon-speech'
    }
  ]
};
