import React, { Component } from 'react';
import ReactTooltip from 'react-tooltip'

class ActionCell extends React.Component {
  constructor(props) {
    super(props);
    this.iconStyle = {
        cursor: "pointer",
        fontSize: "20px",
        margin: "0px 0px 15px 20px",
      };
  }
  render() {
    return (
      <div>

        <a className="icon-chart"  data-tip data-for='chart' style={this.iconStyle}/>
        <a className="icon-notebook"  data-tip data-for='trade' style={this.iconStyle}/>
        <a className="icon-calendar"  data-tip data-for='event' style={this.iconStyle}/>
        <a className="icon-star"  data-tip data-for='favorite' style={this.iconStyle}/>
        <ReactTooltip id='chart' place="top" type="success" effect="solid">
          <span>View chart</span>
        </ReactTooltip>
        <ReactTooltip id='trade' place="top" type="success" effect="solid">
          <span>Trade</span>
        </ReactTooltip>
        <ReactTooltip id='favorite' place="top" type="success" effect="solid">
          <span>Add to Favorite </span>
        </ReactTooltip>
        <ReactTooltip id='event' place="top" type="success" effect="solid">
          <span>Event</span>
        </ReactTooltip>
      </div>
    );
  }
}


class TableCellFormatter {
  constructor(exchange) {
    // init Market
  }

  static link(cell, row) {
    var url = "https://bittrex.com/Market/Index?MarketName=" + cell;
    return (
      <a target="_blank" type='text' href={ url }>{cell}</a>
    );
  }

  static time(cell, row) {
    if(cell){
      return moment(cell).format('LTS');
    }
  }

  static percent(cell, row) {
    if(cell){
      return cell.toFixed(2) + "%";
    }
  }

  static price(cell, row) {
    if(cell){
      return cell.toFixed(10);
    }
  }

  static action(cell, row) {
    return (
      <ActionCell />
    );
  }

  static thousand(cell, row) {
      var value = cell.toFixed(2);
      value = value > 999999 ? (value/1000).toFixed(1) + 'k' : value;
      return value;
  }

  static numberColor(fieldValue, row, rowIdx, colIdx) {
    return fieldValue > 0 ? 'td-change-positive' : 'td-change-negative';
  }

}

export default TableCellFormatter
