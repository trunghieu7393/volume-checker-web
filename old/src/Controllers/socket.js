
import SocketCluster from 'socketcluster-client';

var api_credentials =
{
    "apiKey"    : "cf464fcf158532a5a5ac3bba8f333857",
    "apiSecret" : "08e8807d093860032174b07ce2765670"
}

var options = {
    hostname  : "sc-02.coinigy.com",
    port      : "443",
    secure    : "true"
};
var SCsocket = SocketCluster.connect(options);

class Socket {

  constructor() {

    SCsocket.on('connect', function (status) {

      console.log('status: ', status);

      SCsocket.on('error', function (err) {
        console.log(err);
      });


      SCsocket.emit("auth", api_credentials, function (err, token) {
        if (!err && token) {
          console.log('Connect to Coinigy Successful');
        } else {
          console.log('Error SCsocket Coinigy: ', err)
        }
      });
    });
  }
  subscribeToMarket() {
    SCsocket.emit("exchanges", null, function (err, data) {
      if (!err) {
          console.log(data);
      } else {
          console.log(err)
      }
    });


    SCsocket.emit("channels", "OK", function (err, data) {
      if (!err) {
          console.log(data);
      } else {
          console.log(err)
      }
    });
  }
}

export default Socket;
