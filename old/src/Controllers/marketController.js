import ccxt from 'ccxt';

class MarketController {
  constructor(exchange) {
    // init Market
    this.exchange = this.getExchange(exchange);
  }
  
  getExchange(exchange) {
    switch (exchange) {
      case 'bittrex':
        return new ccxt.bittrex();
      case 'binance':
        return new ccxt.binance();
      default:
        return new ccxt.bittrex();
    }
  }

  getInfoCoins(callback) {
    return this.exchange.fetchTickers().then(json => json);
  }
}

export default MarketController
