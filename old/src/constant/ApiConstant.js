const API_HOSTNAME = '//localhost';
export const CLIENT_ID = '12345678';

const constructUrl = url => `${API_HOSTNAME}${url}${url.indexOf('?') === -1 ? '?' : '&'}client_id=${CLIENT_ID}`;

export const SESSION_FOLLOWINGS_URL = `${API_HOSTNAME}/me/followings`;
