# Volume Checker Web

Là 1 website theo dõi khối lượng giao dịch, và giá trị thặng dư của cryptocurrency.

## Install

- [nvm](https://github.com/creationix/nvm) quản lý version nodeJS.
- editor/IDE tùy cá nhân.
- cài đặt nodeJS 8.9.1(faster, new syntax,...).

```
npm install

```

# Available Scripts

Trong CLI có thể sử dụng những script sau để run project

### `npm start`

Build và chạy website bằng webpack. Website sẽ chạy trên http://localhost:8000

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.
