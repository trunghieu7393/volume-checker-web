import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { connect } from 'react-redux';

import App from './containers/App';
import asyncComponent from './helpers/AsyncFunc';
import Auth0 from './helpers/auth0';

const RestrictedRoute = ({ component: Component, isLoggedIn, ...rest }) => (
  <Route
    {...rest}
    render={props => isLoggedIn
      ? <Component {...props} />
      : <Redirect
          to={{
            pathname: '/login',
            state: { from: props.location },
          }}
        />}
  />
);

const PublicRoutes = ({ history, isLoggedIn }) => {
  return (
    <ConnectedRouter history={history}>
      <Switch>
          <Route
            exact
            path={'/404'}
            component={asyncComponent(() => import('./containers/Pages/404'))}
          />
          <Route
            exact
            path={'/404'}
            component={asyncComponent(() => import('./containers/Pages/404'))}
          />
          <Route
            exact
            path={'/500'}
            component={asyncComponent(() => import('./containers/Pages/500'))}
          />
          <Route
            exact
            path={'/login'}
            component={asyncComponent(() => import('./containers/Pages/login'))}
          />
          <Route
            exact
            path={'/signup'}
            component={asyncComponent(() => import('./containers/Pages/signup'))}
          />
          <Route
            exact
            path={'/forgotpassword'}
            component={asyncComponent(() =>
              import('./containers/Pages/forgotPassword'))}
          />

          <Route path="/auth0loginCallback" render={props => {
              Auth0.handleAuthentication(props);
            }}
          />
          <RestrictedRoute path="/" component={App} isLoggedIn={isLoggedIn} />
      </Switch>
    </ConnectedRouter>
  );
};

export default connect(state => ({
  isLoggedIn: true, //state.Auth.get('idToken') !== null,
}))(PublicRoutes);
