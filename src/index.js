import React from 'react';
import ReactDOM from 'react-dom';
import { LocaleProvider } from 'antd';
import { IntlProvider } from 'react-intl';
import registerServiceWorker from './registerServiceWorker';
import ReduxMgr from './ReduxMgr';
import AppLocale from './languageProvider';

// TODO: Copy Language Change
const currentAppLocale = AppLocale['en'];

// Create a reusable render method that we can call more than once
let render = () => {
    ReactDOM.render(
      <LocaleProvider locale={currentAppLocale.antd}>
          <IntlProvider
            locale={currentAppLocale.locale}
            messages={currentAppLocale.messages}
          >
              <ReduxMgr />
          </IntlProvider>
      </LocaleProvider>,
      document.getElementById('app-site')
    );

};

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./ReduxMgr', () => {
    const NextApp = require('./ReduxMgr').default;
    ReactDOM.render(<NextApp />, document.getElementById('app-site'));
  });
}

registerServiceWorker();
render();
