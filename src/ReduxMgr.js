import React from 'react';
import { Provider } from 'react-redux';
import { store, history } from './store';
import PublicRoutes from './router';
const ReduxMgr = () =>
    <Provider store={store}>
        <PublicRoutes history={history} />
    </Provider>;

export default ReduxMgr;
