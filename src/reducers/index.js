import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import Settings from './settings/reducer';
import App from './app/reducer';
import Auth from './auth/reducer';
import Volume from './volume/reducer';


const reducers = combineReducers({
    routing: routerReducer,
    App,
    settings: Settings,
    Auth,
    Volume,
});

export default reducers;
