import {FIXED_DRAWER, DARK_THEME, DRAWER_TYPE, THEME_COLOR, TOGGLE_COLLAPSED_NAV} from 'constants/ActionTypes';
import actions, { getView } from './actions';
import { DARK_INDIGO } from './../../constants/ThemeColors';

const initState = {
    navCollapsed: false,
    drawerType: FIXED_DRAWER,
    themeColor: DARK_INDIGO,
    darkTheme: false,
};

export default function appReducer(state = initState, action) {
  switch (action.type) {
    case '@@router/LOCATION_CHANGE':
        return {
            ...state,
            navCollapsed: false
        };
    case TOGGLE_COLLAPSED_NAV:
        return {
            ...state,
            navCollapsed: action.isNavCollapsed
        };
    case DRAWER_TYPE:
        return {
            ...state,
            drawerType: action.drawerType
        };
    case THEME_COLOR:
        return {
            ...state,
            darkTheme: false,
            themeColor: action.color
        };
    case DARK_THEME:
        return {
            ...state,
            darkTheme: !state.darkTheme
        };
    default:
        return state;
  }
  return state;
}
