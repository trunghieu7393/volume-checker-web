import {DARK_THEME, DRAWER_TYPE, THEME_COLOR, TOGGLE_COLLAPSED_NAV} from 'constants/ActionTypes';

const actions = {
  toggleCollapsedNav : (isNavCollapsed) => {
    return {type: TOGGLE_COLLAPSED_NAV, isNavCollapsed};
  },

  setDrawerType : (drawerType) =>  {
      return {type: DRAWER_TYPE, drawerType};
  },

  setThemeColor : (color) => {
      return {type: THEME_COLOR, color};
  },
  setDarkTheme : () => {
      return {type: DARK_THEME};
  },
};
export default actions;
