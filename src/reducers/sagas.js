import { all } from 'redux-saga/effects';
import authSagas from './auth/saga';
import volumeSagas from './volume/saga';

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    volumeSagas()
  ]);
}
