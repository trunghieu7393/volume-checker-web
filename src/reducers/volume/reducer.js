import { Map } from 'immutable';
import actions from './actions';

var initState = new Map({
  coin: '',
  markets: null,
  marketData: null,
  newDayData: null,
  volumeCoin: null,
  surplusCoin: null,
  errors: null,
});


// Modify data
const getMarketList = (data) => {
  var markets = [];
  for(var coin of data){
    try {
        const market = coin._id.split('/')[1];
        if(!markets.includes(market)){
            markets.push(market);
        }
    } catch (error) {
        // TODO: using LOG library
        console.log(error);
    }
  }
  return markets;
}

// Modify data
const calcChangeData = (startdayData, newestData) => {
    var marketData = [];
    for(var coin of startdayData){
      try {
          const openVolume = coin.data.volume.open;
          var coinData = newestData[coin._id];
          coinData.startVolume = openVolume ? openVolume : 0;
          coinData.changeVolume = openVolume ? ((coinData.quoteVolume - openVolume) / openVolume) * 100 : 0;
          coinData.percentage = coinData.percentage ? coinData.percentage : (coinData.change * 100);
          marketData.push(coinData);
      } catch (error) {
        // TODO: using LOG library
        console.log(error);
      }
    }
    return marketData;
}

// Modify data
const calcSurplusData = (data) => {
    var allSurplus = 0;
    const dataSurplus = data.slice(0).reverse().map( record => {
        var result = {};
        result.date = record.date.start;
        var sellSum = record.data.sell ? record.data.sell.sum : 0;
        var buySum = record.data.buy ? record.data.buy.sum : 0;
        const surplus = (buySum - sellSum);
        allSurplus += surplus;
        result.surplus = surplus;
        result.allSurplus = allSurplus;
        return result;
    });
    return dataSurplus;
}

export default function volumeReducer(state = initState, action) {
  switch (action.type) {
    case actions.CHANGE_TRACK_SYMBOL:
        return state.set('coin', action.symbol);

    case actions.NEWDAY_REQUEST_SUCCESS:
        const markets = getMarketList(action.data);
        var newState = state.set('markets', markets);
        return newState.set('newDayData', action.data);
    case actions.MARKET_REQUEST_ERROR:
      return state.set('errors', action.error);

    case actions.MARKET_REQUEST_SUCCESS:
        const newday = state.get('newDayData');
        const marketData = newday ? calcChangeData(newday, action.data) : null;
        return state.set('marketData', marketData);
    case actions.MARKET_REQUEST_ERROR:
        return state.set('errors', action.error);

    case actions.VOLUME_REQUEST_SUCCESS:
        const dataVolume = action.data.slice(0).reverse().map( record => {
            var result = {};
            result.date = record.date.start;
            result.volume = record.data.volume;
            result.price = record.data.price;
            return result;
        });
        return state.set('volumeCoin', dataVolume);
    case actions.VOLUME_REQUEST_ERROR:
        return state.set('errors', action.error);

    case actions.SURPLUS_REQUEST_SUCCESS:
        var allSurplus = 0;
        const dataSurplus = calcSurplusData(action.data);
        return state.set('surplusCoin', dataSurplus);
    case actions.SURPLUS_REQUEST_ERROR:
        return state.set('errors', action.error);
    default:
        return state;
  }
}
