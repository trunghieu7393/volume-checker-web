import { all, takeEvery, put, fork, call} from 'redux-saga/effects';
import { push } from 'react-router-redux';
import actions from './actions';
import { getNewDay, getVolumeSymbol, getMarketData, getSurplusSymbol } from 'api/index';


export function* newdayRequest(param) {
  yield takeEvery(actions.REQUEST_NEWDAY_DATA, function*(param) {
    const { exchange, token } = param.payload;
    if(typeof exchange === undefined || exchange ==='' ){
      yield put(actions.volumeRequestError('Cannot request without Exchange code'));
    }
    try {
      const requestResult = yield call(
        getNewDay,
        exchange,
        token ? `&token=${token}` : ''
      );

      if (requestResult.status == 200) {
        yield put(
          actions.newDayRequestSuccess(requestResult.data.result)
        );
      } else {
        yield put(actions.newDayRequestError(requestResult));
      }
    } catch (error) {
      yield put(actions.newDayRequestError(error));
    }
  });
}

export function* marketDataRequest(param) {
  yield takeEvery(actions.REQUEST_MARKET_DATA, function*(param) {
    const { exchange } = param.payload;
    if(typeof exchange === undefined || exchange ==='' ){
        yield put(actions.volumeRequestError('Cannot request without Exchange code'));
        return;
    }
    try {
      const requestResult = yield call(
        getMarketData,
        exchange
      );
      yield put(
        actions.marketRequestSuccess(requestResult)
      );
    } catch (error) {
      yield put(actions.marketRequestError(error));
    }
  });
}

export function* volumeRequest(param) {
  yield takeEvery(actions.REQUEST_VOLUME_DATA, function*(param) {
    const { exchange, token, interval, symbol, limit } = param.payload;
    if(typeof exchange === undefined || exchange ==='' ){
        yield put(actions.volumeRequestError('Cannot request without Exchange code'));
        return;
    }
    try {
      const requestResult = yield call(
        getVolumeSymbol,
        exchange,
        symbol ? `?symbol=${symbol}` : '',
        interval ? `&interval=${interval}` : '',
        limit ? `&limit=${limit}` : '',
        token ? `&token=${token}` : ''
      );
      if (requestResult.status == 200) {
        yield put(
          actions.volumeRequestSuccess(requestResult.data.result)
        );
        yield put(actions.changeTrackSymbol(symbol) );
      } else {
        yield put(actions.volumeRequestError(requestResult));
      }
    } catch (error) {
      yield put(actions.volumeRequestError(error));
    }
  });
}

export function* surplusRequest(param) {
  yield takeEvery(actions.REQUEST_SURPLUS_DATA, function*(param) {
    const { exchange, token, interval, symbol, limit } = param.payload;
    if(typeof exchange === undefined || exchange ==='' ){
        yield put(actions.volumeRequestError('Cannot request without Exchange code'));
        return;
    }
    try {
      const requestResult = yield call(
        getSurplusSymbol,
        exchange,
        symbol ? `?symbol=${symbol}` : '',
        interval ? `&interval=${interval}` : '',
        limit ? `&limit=${limit}` : '',
        token ? `&token=${token}` : ''
      );
      if (requestResult.status == 200) {
        yield put(
          actions.surplusRequestSuccess(requestResult.data.result)
        );
      } else {
        yield put(actions.surplusRequestError(requestResult));
      }
    } catch (error) {
      yield put(actions.surplusRequestError(error));
    }
  });
}

export default function* rootSaga() {
  yield all([
    // using fork to run in background
    fork(newdayRequest),
    fork(marketDataRequest),
    fork(volumeRequest),
    fork(surplusRequest)
  ]);
}
