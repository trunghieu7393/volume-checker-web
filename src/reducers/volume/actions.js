const actions = {
    /*
    * NEWDAY
    */
    CHANGE_TRACK_SYMBOL: 'CHANGE_TRACK_SYMBOL',
    changeTrackSymbol: (symbol) => ({
      type: actions.CHANGE_TRACK_SYMBOL,
      symbol : symbol
    }),


    /*
    * NEWDAY
    */
    REQUEST_NEWDAY_DATA: 'REQUEST_NEWDAY_DATA',
    requestNewday: (payload) => ({
      type: actions.REQUEST_NEWDAY_DATA,
      payload : payload
    }),
    NEWDAY_REQUEST_SUCCESS: 'NEWDAY_REQUEST_SUCCESS',
    newDayRequestSuccess: (result) => ({
      type: actions.NEWDAY_REQUEST_SUCCESS,
      data : result,
    }),
    NEWDAY_REQUEST_ERROR: 'NEWDAY_REQUEST_ERROR',
    newDayRequestError: (error) => ({
      type: actions.NEWDAY_REQUEST_ERROR,
      error : error,
    }),

    /*
    * MARKET
    */
    REQUEST_MARKET_DATA: 'REQUEST_MARKET_DATA',
    requestMarketData: (payload) => ({
      type: actions.REQUEST_MARKET_DATA,
      payload : payload
    }),
    MARKET_REQUEST_SUCCESS: 'MARKET_REQUEST_SUCCESS',
    marketRequestSuccess: (result) => ({
      type: actions.MARKET_REQUEST_SUCCESS,
      data : result
    }),
    MARKET_REQUEST_ERROR: 'MARKET_REQUEST_ERROR',
    marketRequestError: (error) => ({
      type: actions.MARKET_REQUEST_ERROR,
      error : error
    }),

    /*
    * VOLUME
    */
    REQUEST_VOLUME_DATA: 'REQUEST_VOLUME_DATA',
    requestVolumeData: (payload) => ({
      type: actions.REQUEST_VOLUME_DATA,
      payload : payload
    }),
    VOLUME_REQUEST_SUCCESS: 'VOLUME_REQUEST_SUCCESS',
    volumeRequestSuccess: (result) => ({
      type: actions.VOLUME_REQUEST_SUCCESS,
      data : result
    }),
    VOLUME_REQUEST_ERROR: 'VOLUME_REQUEST_ERROR',
    volumeRequestError: (error) => ({
      type: actions.VOLUME_REQUEST_ERROR,
      error : error
    }),

    /*
    * VOLUME
    */
    REQUEST_SURPLUS_DATA: 'REQUEST_SURPLUS_DATA',
    requestSurplusData: (payload) => ({
      type: actions.REQUEST_SURPLUS_DATA,
      payload : payload
    }),
    SURPLUS_REQUEST_SUCCESS: 'SURPLUS_REQUEST_SUCCESS',
    surplusRequestSuccess: (result) => ({
      type: actions.SURPLUS_REQUEST_SUCCESS,
      data : result
    }),
    SURPLUS_REQUEST_ERROR: 'SURPLUS_REQUEST_ERROR',
    surplusRequestError: (error) => ({
      type: actions.SURPLUS_REQUEST_ERROR,
      error : error
    }),

};
export default actions;
