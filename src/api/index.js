import axios from 'axios';
import ccxt from 'ccxt';
import { EXCHANGE_LIST } from 'constants/exchange';

var apiUrl = 'http://35.231.60.183:3000';

// Create exchange support list
var exchangeList = {};
for (var title of EXCHANGE_LIST) {
  var exchange;
  switch (title) {
    case 'bittrex':
      exchange = new ccxt.bittrex();
      break;
    case 'binance':
      exchange = new ccxt.binance();
      break;
    default:
      exchange = null;
  }
  exchangeList[title] = exchange;
}

export const getNewDay = async (exchange, token) => {
  const url = apiUrl + '/api/v1.0/' + exchange + '/getVolumeNewDay' + token;
  return axios.get(url)
   .then(res => res)
   .catch(error => error);
}

/*
* MARKET DATA REQUEST
*/
export const getMarketData = async (exchange) => {
  return exchangeList[exchange].fetchTickers()
   .then(res => res)
   .catch(error => error);
}

/*
* VOLUME REQUEST
*/
export const getVolumeSymbol = async (exchange, symbol, interval, limit, token) => {
  const url = apiUrl + '/api/v1.0/' + exchange + '/getVolume' + symbol + interval + limit + token;
  return axios.get(url)
   .then(res => res)
   .catch(error => error);
}

/*
* SURPLUS REQUEST
*/
export const getSurplusSymbol = async (exchange, symbol, interval, limit, token) => {
  const url = apiUrl + '/api/v1.0/' + exchange + '/getSurplus' + symbol + interval + limit + token;
  return axios.get(url)
   .then(res => res)
   .catch(error => error);
}
