const BITTREX_EXCHANGE = 'bittrex';
const BINANCE_EXCHANGE = 'binance';

export const EXCHANGE_LIST = [
  BITTREX_EXCHANGE,
  BINANCE_EXCHANGE
]
