import moment from 'moment';
class TableCellFormatter {
    constructor(exchange) {
      // init Market
    }

    static time(params) {
        return moment(params.value).format('LTS');
    }
    static date(time) {
      	return moment(time).format('YYYY MMMM Do, h:mm:ss a');
    }

    static percent(params) {
        return params.value.toFixed(2) + '%';
    }

    static price(params) {
        return params.value.toFixed(8);
    }

    static thousand(params) {
        var value = params.value.toFixed(2);
        value = value > 999999 ? (value/1000).toFixed(1) + 'k' : value;
        return value;
    }

    static numberColor(fieldValue, row, rowIdx, colIdx) {
        return fieldValue > 0 ? 'td-change-positive' : 'td-change-negative';
    }

    static thousandAxis(value) {
        var value = value.toFixed(2);
        value = value > 999999 ? (value/1000).toFixed(1) + 'k' : value;
        return value;
    }

    static priceAxis(value) {
        return value.toFixed(8);
    }

    static hourAxis (time) {
    	 return moment(time).format('HH');
    };

}

export default TableCellFormatter
