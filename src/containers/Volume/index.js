import React, { Component } from 'react';
import { connect } from 'react-redux';
// import ReactGridLayout from 'react-grid-layout';
import RGL, {WidthProvider} from 'react-grid-layout';

import ContainerHeader from 'components/ContainerHeader';
import actions from 'reducers/volume/actions';
import VolumeTbl from 'components/VolumeTbl/index';
import VolumeChart from 'components/VolumeChart/index';

const { requestNewday, requestMarketData, requestVolumeData, requestSurplusData } = actions;
const ReactGridLayout = WidthProvider(RGL);

class Volume extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    
    componentWillReceiveProps(nextProps){
        if(nextProps.newDayData && !nextProps.marketData){
            // run each 30s
            var that = this;
            this.props.requestMarketData({exchange: 'bittrex'});
            setInterval(function () {
                that.props.requestMarketData({exchange: 'bittrex'});
            }, 45000);
        }
    }
    

    // Override Method: run on Onload
    componentDidMount() {
        this.props.requestNewday({exchange : 'bittrex'});
    }
    /* Grid Events we're listening to */
    onRowSelected = (event) => {
        const params = {
          exchange: 'bittrex',
          symbol: event.node.data.symbol,
          interval: 'hour'
        }
        this.props.requestVolumeData(params);
        this.props.requestSurplusData(params);
    };
    
    // <ContainerHeader title="Volume Analysis" match={'match'}/>
    render(){
        return (
            <div className="animated slideInUpTiny animation-duration-3">
                <ContainerHeader title="Volume Analysis" match={this.props.match}/>
                <div className="row mx-auto">
                    <div className="p-2 col-lg-6 col-sm-12" style={{height:'850px'}}>
                        <VolumeTbl data={this.props.marketData} onRowSelected={this.onRowSelected} filters={this.props.markets}/>
                    </div>
                    <div className="p-2 col-lg-6 col-sm-12" style={{height:'850px'}}>
                        <VolumeChart volumeCoin={this.props.volumeCoin} surplusCoin={this.props.surplusCoin} coin={this.props.coin}/>
                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    const { markets, newDayData, marketData, volumeCoin, surplusCoin, coin } = state.Volume.toJS();
    return { markets, newDayData, marketData, volumeCoin, surplusCoin, coin };
}
const mapDispatchToProps = { requestNewday, requestMarketData, requestVolumeData, requestSurplusData }

export default connect(mapStateToProps, mapDispatchToProps)(Volume);
