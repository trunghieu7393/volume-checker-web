import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import {AgGridReact, AgGridColumn} from 'ag-grid-react';
import actions from 'reducers/volume/actions';
import cellFormatter from 'helpers/cellFormatter';
import CardBox from 'components/CardBox/index';
import {MenuItem} from 'material-ui/Menu';
import Select from 'material-ui/Select';

// Loading css
import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/ag-theme-blue.css';

class VolumeTbl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            filter: '',
            columnDefs: this.createColumnDefs(),
            rowData: null,
            style: {
                width: '100%',
                height: '100%'
            },
            sortingOrder: ['desc', 'asc', null],
            rowSelection: 'single'
        };
    };
    createColumnDefs() {
        return [
            {
                headerName: 'Symbol',
                field: 'symbol',
                width: 120,
                filter: 'agTextColumnFilter',
                sortingOrder: ['asc', 'desc'],
                getQuickFilterText: function(params) {
                    return params.value;
                }
            },
            {
                headerName: 'Price',
                field: 'last',
                valueFormatter: cellFormatter.price,
                // suppressFilter: true,
                width: 110,
                sortingOrder: ['asc', 'desc'],
                cellClass: 'number-cell'
            },
            {
                headerName: '%Price',
                field: 'percentage',
                valueFormatter: cellFormatter.percent,
                // suppressFilter: true,
                width: 100,
                sortingOrder: ['asc', 'desc'],
                cellClass: 'number-cell'
            },
            {
                headerName: '0h Vol',
                field: 'startVolume',
                valueFormatter: cellFormatter.thousand,
                // suppressFilter: true,
                width: 120,
                sortingOrder: ['asc', 'desc'],
                cellClass: 'number-cell'
            },
            {
                headerName: 'Vol',
                field: 'quoteVolume',
                valueFormatter: cellFormatter.thousand,
                // suppressFilter: true,
                width: 100,
                sortingOrder: ['asc', 'desc'],
                cellClass: 'number-cell'
            },
            {
                headerName: '%Vol',
                field: 'changeVolume',
                valueFormatter: cellFormatter.percent,
                // suppressFilter: true,
                width: 120,
                sortingOrder: ['asc', 'desc'],
                sort: 'desc',
                cellClass: 'number-cell'
            }
        ];
    };

    componentWillReceiveProps(nextProps){
        const filters = nextProps.filters;
        if(this.state.items.length == 0 && filters){
            for (let i = 0, length = filters.length; i < length; i++ ) {
                this.state.items.push(<MenuItem value={filters[i]} key={i}>{filters[i]}</MenuItem>);
            }
            // Set First Select
            this.setState({filter: filters[0]});
            this.gridApi.setQuickFilter(filters[0]);
        }
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.columnApi = params.columnApi;
    }

    handleChange = key => event => {
        this.setState({[key]: event.target.value});
        this.gridApi.setQuickFilter(event.target.value);
    };
    
    externalFilter = node => event => {
        return node.data.symbol.includes(this.state.filter);
    };
    
    render() {
        return (
            <CardBox heading="Cryptocurrency Table" styleName="col-lg-12 col-sm-12 h-100" cardStyle='h-100' childrenStyle='h-100'>
                <Select
                    value={this.state.filter}
                    onChange={this.handleChange('filter')}
                >
                    {this.state.items}
                </Select>
                <div ag-grid="gridOptions" className="ag-theme-blue" style={{boxSizing: 'border-box', height: '90%', width: '100%'}}>
                    <div style={this.state.style}>
                        <AgGridReact
                            // listening for events
                            // events
                            onGridReady={this.onGridReady.bind(this)}
                            onRowSelected={this.props.onRowSelected.bind(this)}
                            // binding to array properties
                            columnDefs={this.state.columnDefs}
                            rowData={this.props.data}
                            sortingOrder={this.state.sortingOrder}
                            rowSelection={this.state.rowSelection}
                            doesExternalFilterPass={this.externalFilter}
                            enableCellChangeFlash={true}
                            enableFilter={true}
                            animateRows={true}
                            floatingFilter={true}
                            valueCache={true}
                            enableSorting>

                            <AgGridColumn field="symbol"></AgGridColumn>
                            <AgGridColumn field="last"></AgGridColumn>
                            <AgGridColumn field="change"></AgGridColumn>
                            <AgGridColumn field="startVolume"></AgGridColumn>
                            <AgGridColumn field="quoteVolume"></AgGridColumn>
                            <AgGridColumn field="changeVolume"></AgGridColumn>
                        </AgGridReact>
                    </div>
                </div>
            </CardBox>
        )
    }
}

export default VolumeTbl;
