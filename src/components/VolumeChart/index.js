import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import {
    Area, AreaChart, Brush, CartesianGrid, Line, LineChart, Legend,
    ResponsiveContainer, Tooltip, XAxis, YAxis, ReferenceLine
} from 'recharts';
import cellFormatter from 'helpers/cellFormatter';

import CardBox from 'components/CardBox/index';
class VolumeChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            surplusData: [],
            coinData: [],
        };
    };
    
    componentWillReceiveProps(nextProps){
        if(nextProps.surplusCoin){
            const arrayLength = nextProps.surplusCoin.length;
            this.state.surplusData = nextProps.surplusCoin.slice(arrayLength/2, arrayLength);
        }
        if(nextProps.coin){
            const arrayLength = nextProps.surplusCoin.length;
            this.state.coinData = nextProps.volumeCoin.slice(arrayLength/2, arrayLength);
        }
    }
    renderLegend = (props) => {
        const { payload } = props;
        return (
          <ul>
              <li key={'50h'}>{payload.allSurplus}</li>
              <li key={'1h'}>{payload.surplus}</li>
          </ul>
        );
      }

    render(){
      return (
            <div className="row h-100">
                <CardBox heading={`Surplus ${this.props.coin}`} styleName="col-lg-12 col-sm-12">
                    <ResponsiveContainer width="100%" height={200}>
                        <LineChart data={ this.state.surplusData} syncId="anyId"
                                margin={{top: 10, right: 10, left: 10, bottom: 0}}>
                            <XAxis dataKey="date" tickFormatter={cellFormatter.hourAxis} padding={{left: 20, right: 20}} unit='h'/>
                            <YAxis tickFormatter={cellFormatter.thousandAxis}/>
                            <CartesianGrid strokeDasharray="3 3" />
                            <Tooltip  labelFormatter={cellFormatter.date}/>
                            <ReferenceLine y={0} stroke="red" />
                            <Legend verticalAlign="top" height={36} align="right"/>
                            <Line name='1h' type="monotone" dataKey="surplus" stroke="#3367d6" activeDot={{r: 8}}/>
                            <Line name='50h' type="monotone" dataKey="allSurplus" stroke="#ffc658"  activeDot={{r: 8}}/>
                        </LineChart>
                    </ResponsiveContainer>
                </CardBox>
                <CardBox heading={`Volume ${this.props.coin}`} styleName="col-lg-12 col-sm-12">
                    <ResponsiveContainer width="100%" height={200}>
                        <LineChart data={this.state.coinData} syncId="anyId"
                                margin={{top: 10, right: 10, left: 10, bottom: 0}}>
                            <XAxis dataKey="date" tickFormatter={cellFormatter.hourAxis} padding={{left: 20, right: 20}} unit='h'/>
                            <YAxis tickFormatter={cellFormatter.thousandAxis}/>
                            <CartesianGrid strokeDasharray="3 3" />
                            <Tooltip labelFormatter={cellFormatter.date}/>
                            <Line name="Volume" type="monotone" dataKey="volume.close" stroke="#3367d6" activeDot={{r: 8}} />
                        </LineChart>
                    </ResponsiveContainer>
                </CardBox>
                <CardBox heading={`Price ${this.props.coin}`} styleName="col-lg-12 col-sm-12">
                    <ResponsiveContainer width="100%" height={200}>
                        <LineChart data={this.state.coinData} syncId="anyId"
                                   margin={{top: 10, right: 10, left: 10, bottom: 0}}>
                            <XAxis dataKey="date" tickFormatter={cellFormatter.hourAxis} padding={{left:20, right: 20}} unit='h'/>
                            <YAxis tickFormatter={cellFormatter.priceAxis}/>
                            <CartesianGrid strokeDasharray="5 5" />
                            <Tooltip labelFormatter={cellFormatter.date}/>
                            <Line name="Price" type="monotone" dataKey="price.close" stroke="#3367d6" activeDot={{r: 8}} />
                        </LineChart>
                    </ResponsiveContainer>
                </CardBox>
            </div>
      )
    }
}

export default VolumeChart;
